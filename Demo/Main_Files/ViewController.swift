
import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        updateDatabase()
        updateStatsView()
    }
}

private extension ViewController {
    
    func updateDatabase() {
        let date = Date()
        for i in 1 ... 1000 {
            if let date = NSCalendar.current.date(byAdding: .second, value: -i, to: date) {
                let interval = Interval(beginTime: date, state: .train)
                interval.save()
            }
        }
    }
    
    func updateStatsView() {
        
        // Print all the intervals dates
        let arrayOfTrainingDays = DB.getArrayOfTrainingDays()
        print(arrayOfTrainingDays) // should be on the main thread
        
        
        // Print first training interval begin time from today
        let todayTrainingIntervals = DB.getTodayTrainingIntervals()
        if let todayTrainingInterval = todayTrainingIntervals.first {
            print(todayTrainingInterval.beginTime) // should be on the main thread
        }
        
        
        // Print first training and tracking intervals begin time from yesterday
        if let yesterday = NSCalendar.current.date(byAdding: .day, value: -1, to: Date()) {
            let trainIntervals = DB.getIntervalsInDate(yesterday, withStateFilter: .train).sorted(byKeyPath: "beginTime", ascending: true)
            let trackIntervals = DB.getIntervalsInDate(yesterday, withStateFilter: .track).sorted(byKeyPath: "beginTime", ascending: true)
            
            if let trainInterval = trainIntervals.first {
                print(trainInterval.beginTime) // should be on the main thread
            }
            if let trackInterval = trackIntervals.first {
                print(trackInterval.beginTime) // should be on the main thread
            }
        }
    }
}
